<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $gallery = File::files(public_path());
        return view('welcome', compact(['gallery']));
    }
}
