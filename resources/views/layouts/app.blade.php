<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">

    <title>Red1Group</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald:400,500,600&display=swap&subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css?v=2.0') }}">
    <link rel="stylesheet" href="{{ asset('css/media.css?v=2.0') }}">
</head>
<body>
    <div class="loading"></div>
    <div class="container-fluid full-height container-scroll">
        <div class="main-row" style="will-change:transform">
            <div class="sidebar border-right">
                <a href="/" class="logo">
                    <img src="{{ asset('img/logo.png') }}" alt="Logo" width="180">
                </a>
                <ul class="sidebar-nav">
                    <li class="sidebar-item">
                        <a href="#about">О нас</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#strategy">Стратегия</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#branding">Брендинг</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#events">Деловые события</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#advertising">Наружная реклама</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#projects">Мультимедийные проекты</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#web">Разработка сайтов</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#partners">Партнеры</a>
                    </li>
                    <li class="sidebar-item">
                        <a href="#contacts">Контакты</a>
                    </li>
                </ul>
                <div class="sidebar-contacts mt-5">
                    <a href="mailto:info@redgroup.kz" class="sidebar-mail">info@redgroup.kz</a>
                    <a href="tel:77899874566" class="sidebar-phone">+7 (777) 262-6254</a>
                </div>
            </div>
            <div class="main-content">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.touchSwipe.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
