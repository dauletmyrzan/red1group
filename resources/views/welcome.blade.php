@extends('layouts.app')

@section('content')
    <div class="columns">
        <div class="column full-height" style="left:0;" id="about" data-left="0">
            <div class="column-head">
                <span class="letter">R</span>
            </div>
            <div class="column-body">
                <h3>О НАС</h3>
                <p><span class="text-danger font-weight-bold">RED ONE GROUP</span> – узнаваемый игрок на рынке рекламных и дизайнерских услуг.</p>
                <div class="hidden-text">
                    <p>Реализуя проекты в сфере организации частных мероприятий, мы заработали положительную репутацию среди своих клиентов, которые теперь доверяют нам в управление корпоративные маркетинговые проекты.</p>
                    <ul>
                        <li>12 лет опыта</li>
                        <li>> 500 реализованных проекта</li>
                        <li>20 профессионалов в команде</li>
                        <li>30 фрилансеров</li>
                    </ul>
                </div>
            </div>
            <div class="visual visual-about"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:40vh;" id="strategy" data-left="40">
            <div class="column-head">
                <span class="letter">E</span>
            </div>
            <div class="column-body">
                <h3>8 причин работать с нами</h3>
                <div class="hidden-text">
                    <ul class="list-style-dash">
                        <li>Креативный подход в разработке мероприятия</li>
                        <li>Международный опыт работы</li>
                        <li>Ведение проекта</li>
                        <li>Исследования</li>
                        <li>Эксклюзивные сценарии</li>
                        <li>Личный менеджер для каждого клиента</li>
                        <li>Минимальные сроки подготовки проекта</li>
                        <li>Аналитика</li>
                    </ul>
                </div>
            </div>
            <div class="visual visual-strategy"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:80vh;" id="branding" data-left="80">
            <div class="column-head">
                <span class="letter">D</span>
            </div>
            <div class="column-body">
                <h3>БРЕНДИНГ</h3>
                <p>Люди покупают не то, что вы создаете, а то, почему вы это создаете.</p>
                <div class="hidden-text">
                    <p>У нас нет цели создать красивое портфолио или выиграть рекламный фестиваль за счет клиента, мы создаем идеи и дизайн для успеха реального бизнеса.</p>
                    <p>Основным нашим преимуществом является глубокая проработка бренда с точки зрения стратегии и позиционирования. Обратившись к нам, компания получает полный комплекс услуг, начиная с разработки платформы бренда, названия, заканчивая созданием всех визуальных кодов и носителей фирменного стиля.</p>
                </div>
            </div>
            <div class="visual visual-branding"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:120vh;" id="partners" data-left="120">
            <div class="column-head text-center">
                <span class="letter letter-one"></span>
            </div>
            <div class="column-body">
                <h3 class="text-center">Наши партнеры</h3>
                <div class="hidden-text">
                    <div class="mt-3 grid clearfix">
                        <img src="{{ asset('img/partners/1.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/2.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/3.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/4.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/5.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/6.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/7.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/8.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/9.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/10.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/11.png') }}" alt="Партнеры">
                        <img src="{{ asset('img/partners/12.png') }}" alt="Партнеры">
                        {{--<img src="{{ asset('img/partners/13.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/14.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/15.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/16.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/17.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/18.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/19.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/20.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/21.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/22.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/23.png') }}" alt="Партнеры">--}}
                        {{--<img src="{{ asset('img/partners/24.png') }}" alt="Партнеры">--}}
                    </div>
                </div>
            </div>
            <div class="visual visual-partners"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:160vh;" id="events" data-left="160">
            <div class="column-head text-center">
                <span class="letter">G</span>
            </div>
            <div class="column-body">
                <h3>ДЕЛОВЫЕ СОБЫТИЯ</h3>
                <p>Выбирая цель, цельтесь выше, не боясь попасть в звёзды.</p>
                <div class="hidden-text">
                    <p>Презентация для партнеров, бизнес встреча или пресс-завтрак, — сегодня это часть фирменного
                        стиля компании, отражение её имиджа и репутации. Потому для организации подобных мероприятий
                        необходим надежный и опытный партнер. Планируя мероприятие, сосредоточьтесь на встречах и
                        бизнес целях.</p>
                    <p>Все прочие организационные вопросы с легкостью решат наши специалисты.</p>
                </div>
            </div>
            <div class="visual visual-events"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:200vh;" id="advertising" data-left="200">
            <div class="column-head text-center">
                <span class="letter">R</span>
            </div>
            <div class="column-body">
                <h3>НАРУЖНАЯ РЕКЛАМА</h3>
                <p>Единственная цель рекламы — продажа товара.</p>
                <div class="hidden-text">
                    В нашей команде работают высококвалифицированные и опытные специалисты, которые отлично разбираются
                    в своем деле. Имея собственную производственную базу, мы с легкостью воплотим в жизнь любые идеи.
                </div>
            </div>
            <div class="visual visual-outdoor"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:240vh;" id="projects" data-left="240">
            <div class="column-head text-center">
                <span class="letter">O</span>
            </div>
            <div class="column-body">
                <h3>МУЛЬТИМЕДИЙНЫЕ ПРОЕКТЫ</h3>
                <p>Аудиовизуальные и интерактивные решения.</p>
                <div class="hidden-text">
                    <p>Мы исследуем инновационные аудиовизуальные формы коммуникации и интерактивные инструменты для
                        эффективного воздействия и овладения вниманием человека.</p>
                    <p>Разработаем и внедрим уникальные и эффективные решения в таких областях как реклама,
                        развлекательные и образовательные мероприятия, публичные события, шоу различного масштаба и
                        прочее. Наша цель - предоставить новый опыт, который расширяет границы творчества и технологий
                        в каждом проекте.</p>
                    <div class="my-2 position-relative">
                        <div class="row">
                            <div class="col-md-6">
                                <iframe class="mb-3" width="100%" height="220" src="https://www.youtube.com/embed/V0nx9TZmk4E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe class="mb-3" width="100%" height="220" src="https://player.vimeo.com/video/151357105" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                            <div class="col-md-6">
                                <iframe class="mb-3" width="100%" height="220" src="https://www.youtube.com/embed/Qr18VzbDiA4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                <iframe class="mb-3" width="100%" height="220" src="https://player.vimeo.com/video/138722076" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column full-height" style="left:280vh;" id="web" data-left="280">
            <div class="column-head text-center">
                <span class="letter">U</span>
            </div>
            <div class="column-body">
                <h3>РАЗРАБОТКА САЙТОВ</h3>
                <p>Основными этапами процесса являются веб-дизайн, вёрстка страниц, программирование для веб на стороне клиента и сервера, а также конфигурирование веб-сервера.</p>
            </div>
            <div class="visual visual-web"></div>
            <div class="column-footer"></div>
        </div>
        <div class="column full-height" style="left:320vh;" id="contacts" data-left="320">
            <div class="column-head text-center">
                <span class="letter">P</span>
            </div>
            <div class="column-body">
                <h3>КОНТАКТЫ</h3>
                <div>
                    <div>Казахстан, ул. Кабанбай батыра, 76</div>
                    <div class="mt-3">Телефоны:</div>
                    <div><a href="tel:+77772626254">+7 (777) 262-6254</a></div>
                    <div><a href="tel:+77022946913">+7 (702) 294-6913</a></div>
                    <div class="mt-3">Email: <a href="mailto:info@red1group.kz">info@red1group.kz</a></div>
                </div>
            </div>
            <div class="visual visual-contacts"></div>
        </div>
        <div id="left_btn"></div>
    </div>
@endsection